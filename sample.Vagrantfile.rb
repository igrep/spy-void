# -*- mode: ruby -*-
# vi: set ft=ruby :

PROJECT_DIRECTORY = File.absolute_path(File.join __dir__, '..')

VM_USERNAME = 'vm'.freeze
VM_PASSWORD = 'vm'.freeze

Vagrant.configure("2") do |config|
  config.vm.box = "box_name"

  config.vm.box_check_update = false

  config.ssh.username = VM_USERNAME
  config.ssh.password = VM_PASSWORD

  config.vm.network "forwarded_port", guest: 8080, host: 8080
  config.vm.network "forwarded_port", guest: 8000, host: 8000

  config.vm.provider "virtualbox" do |vb|
    vb.gui = true
    vb.memory = "2048"
  end

  config.vm.provision "ASSETS", type: "file", source: "assets/vagrant/", destination: "~/"
  config.vm.provision "CHMOD-X", type: "shell" do|s|
    s.privileged = false
    s.inline = "chmod +x ~/assets/*.sh"
  end

  config.vm.provision "CREATE-PROJECT-DIRECTORY", type: "shell" do|s|
    s.privileged = false
    s.inline = "mkdir -p ~/project"
  end

  config.vm.synced_folder ".", "/vagrant", disabled: true

  %w(
    shared1
    shared2
    shared3
  ).each do|dir|
    host_path = "#{PROJECT_DIRECTORY}/#{dir}/"
    config.vm.synced_folder host_path, "/home/vm/project/#{dir}/"
  end
end
