directory "/path/to/directory" do
  action :create
end

execute "create an empty file" do
  command "touch /path/to/file"
  not_if "test -e /path/to/file"
end
