=begin
SpyVoid - Tiny class to collect configuration information written in Ruby.

@example

  Given a Vagrantfile, a configuration file written in Ruby:

  require 'spy-void'

  Vagrant = SpyVoid.new

  load './Vagrantfile'

  # Get OpenStruct containing all called methods with their arguments.
  log = SpyVoid.retrieve_log Vagrant
  log.configure # => [["2"]]
  log.provider  # => [['provider1], ['provider2']]
  log.provision # => [['shell', { inline: 'echo provisioning!' }]]
=end

require 'ostruct'

class SpyVoid
  def initialize
    @__log = ::OpenStruct.new
  end

  # Log any method calls with their arguments.
  # If a block given, the block is called with +self+ as its required arguments.
  #
  # @return SpyVoid +self+
  #
  def method_missing method_name, *args, &block
    name = method_name
    method_name_s = method_name.to_s
    if method_name_s.sub!(/=$/, ''.freeze)
      name = method_name_s.to_sym
    end
    @__log[name] ||= []
    @__log[name] << args
    unless block.nil?
      arity = 
        if block.arity >= 0
          block.arity
        else
          required = ~block.arity
          required + 1
        end
      yield(*Array.new(arity, self))
    end
    self
  end

  def self.retrieve_log spy
    spy.instance_eval { @__log }
  end

  # Spy into a file.
  # Used to extract information from a file with
  # methods called by implicit receiver.
  # e.g. Itamae, RSpec, etc.
  def self.spy_file path
    instance = self.new
    instance.instance_eval(::IO.read path)
    instance
  end
end

if __FILE__ == $PROGRAM_NAME
  require 'pp'

  puts "Sample 1: Vagrantfile"

  Vagrant = SpyVoid.new
  load('./sample.Vagrantfile.rb')
  pp SpyVoid.retrieve_log(Vagrant).to_h

  puts ("-" * 80)
  puts "Sample 2: Itamae recipe"
  pp SpyVoid.retrieve_log(SpyVoid.spy_file('./sample.itamae.rb')).to_h
end
